
#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]


 //' @title Likelihood contribution of time t for ACB model with Gaussian innovations for covariance estimation.
 //' @description This function computes the likelihood contributions for every time t
 //' @param pars Vector of parameters (param.cst, GARCH.pars, param.tv)
 //' @param fixed_id vector specifying the position of parameters to be fixed
 //' @param est_id vector specifying the position of parameters to be estimated
 //' @param fixed_vals vector of fixed values to be imposed in the pars vector
 //' @param y Dependent variable (vector)
 //' @param x_c Regressor with constant parameter (matrix)
 //' @param x_het Heteroskedastic regressor with time-varying param (matrix)
 //' @param x_hom Homoskedastic regressors with time-varying param (matrix)
 //' @param z Exogenous variables (matrix)
 //' @param init initialization of the filters (vector)
 //' @param mu constant conditional mean of regressors x.tv.het (vector)
 //' @param condvar conditional volatilities of heterosked. reg x.tv.het (matrix)
 //' @param p_cst number of constant parameters
 //' @param p_tv_het number of time-varying parameters of heteroskedastic reg.
 //' @param p_tv_hom number of time-varying parameters of homoskedastic reg.
 //' @param npar_GARCH number of GARCH parameters
 //' @param npar_tv number of parameters in tv param filter
 //' @param q number of exogenous variables
 //' @param k_full number of parameters in ACB in full parameter case
 //' @importFrom Rcpp sourceCpp
 //' @useDynLib ACB
 //' @keywords internal
 //' @export
 // [[Rcpp::export]]
arma::mat ACB_likl_contr(arma::vec pars, arma::vec fixed_id, arma::vec est_id,
                       arma::vec fixed_vals, arma::colvec y,
                       arma::mat x_c, arma::mat x_het, arma::mat x_hom, arma::mat z,
                       arma::vec init, arma::colvec mu, arma::mat condvar,
                       double p_cst, double p_tv_het, double p_tv_hom,
                       double npar_GARCH, double npar_tv, double q, double k_full)
 {

   int n = y.n_elem;                            // number of observations
   //int p = p_cst + p_tv_het + p_tv_hom;       // number of regressors

   int p_tv = p_tv_het + p_tv_hom;  // number of reg. with time-varying pars

   // Initialize matrices, vectors and variables
   arma::mat x_cst, x_tv_het, x_tv_hom, pars_cst_vec, pars_cst, pars_tv_vec, pars_tv, pars_tv_het, pars_tv_hom;
   arma::mat param_cst    = arma::zeros<arma::mat>(n, p_cst);
   arma::mat param_tv_het = arma::zeros<arma::mat>(n, p_tv_het);
   arma::mat param_tv_hom = arma::zeros<arma::mat>(n, p_tv_hom);
   arma::mat condvola     = arma::zeros<arma::mat>(n, 1);
   arma::mat res          = arma::zeros<arma::mat>(n, 1);
   arma::mat yhat         = arma::zeros<arma::mat>(n, 1);
   arma::mat pars_full    = arma::zeros<arma::mat>(k_full, 1);
   arma::vec param_tv_0, gamma_vec;
   arma::mat LogL_contr   = arma::zeros<arma::mat>(n, 1);
   double omega, alpha, beta, k_fixed, k_est, res_0, condvola_0, varpi, xi, c;


   // regressors
   if (p_cst > 0){
     x_cst    = x_c;
   }

   if (p_tv_het > 0){
     x_tv_het = x_het;
   }

   if (p_tv_hom > 0){
     x_tv_hom = x_hom;
   }

   if (q > 0){
     z = z;
   }


   // augmented the parameter vector to be estimated by the fixed values

   k_fixed = fixed_id.n_elem;
   k_est   = est_id.n_elem;

   for (int k = 0; k < k_fixed; k++){
     // impose the parameters that are estimated to the pars vector
     int idx = fixed_id(k);

     pars_full.row(idx) =  fixed_vals(k);

   }

   for (int k = 0; k < k_est; k++){
     // impose the fixed values to the pars vector
     int idx = est_id(k);

     pars_full.row(idx) =  pars(k);

   }

   // read out parameters (GARCH_param, param_cst,  param_tv)

   // GARCH parameters
   omega = pars_full(0);
   alpha = pars_full(1);
   beta  = pars_full(2);


   // constant parameters: param_cst
   if (p_cst > 0){
     pars_cst_vec = pars_full.rows(npar_GARCH,(p_cst*(3+q)+npar_GARCH-1));

     // reshape as matrix of dimension (p_cst x npar_tv)
     pars_cst = reshape(pars_cst_vec, npar_tv, p_cst);
     pars_cst = pars_cst.t();
   }

   // time-varying parameters
   if (p_tv > 0){
     pars_tv_vec = pars_full.rows((p_cst*(3+q)+npar_GARCH), (k_full-1));


     // reshape as matrix of dimension (p_tv x npar_tv)
     pars_tv = reshape(pars_tv_vec, npar_tv, p_tv);
     pars_tv = pars_tv.t();


     // separate for heteroskedastic and homoskedastic regressors
     if ((p_tv_het>0)&&(p_tv_hom>0)) {

       pars_tv_het = pars_tv.rows(0,p_tv_het-1);
       pars_tv_hom = pars_tv.rows(p_tv_het,p_tv-1);

     } else if ((p_tv_het>0)&&(p_tv_hom==0)){

       pars_tv_het = pars_tv;

     } else if ((p_tv_het==0)&&(p_tv_hom>0)){

       pars_tv_hom = pars_tv;

     }
   }


   // read out initial values for the filters for t=1
   res_0 = 0.01;     // initial residual is set to zero

   if (p_cst > 0){

     param_cst.row(0) = pars_cst.col(0).t();

   }

   // param_tv filters: first p_tv elements in init
   if (p_tv > 0){

     param_tv_0 = init.subvec(0,p_tv-1);

     if ((p_tv_het>0)&&(p_tv_hom>0)){
       param_tv_het.row(0) = param_tv_0.subvec(0,p_tv_het-1).t();
       param_tv_hom.row(0) = param_tv_0.subvec(p_tv_het,p_tv-1).t();
     } else if ((p_tv_het>0)&&(p_tv_hom==0)){
       param_tv_het.row(0) = param_tv_0.t();
     } else if ((p_tv_het==0)&&(p_tv_hom>0)){
       param_tv_hom.row(0) = param_tv_0.t();
     }

   }

   // conditional mean yhat: impose the impact of the constant parameters on all
   // periods t of yhat if p_cst > 0

   if (p_cst>0){
     for (int m = 0; m < p_cst; m++){

       yhat(0) += param_cst(0,m) * x_cst(0,m);

     }
   }

   // conditional variance of res
   condvola_0 = init(init.n_elem-1);

   // initialize yhat(0)

   if (p_tv > 0){

     if ((p_tv_het>0)&&(p_tv_hom>0)){

       for (int j = 0; j < p_tv_het; j++){

         yhat(0) += param_tv_het(0,j) * x_tv_het(0,j);

       }

       for (int k = 0; k < p_tv_hom; k++){

         yhat(0) += param_tv_hom(0,k) * x_tv_hom(0,k);

       }


     } else if ((p_tv_het>0)&&(p_tv_hom==0)){

       for (int j = 0; j < p_tv_het; j++){

         yhat(0) += param_tv_het(0,j) * x_tv_het(0,j);

       }

     } else if ((p_tv_het==0)&&(p_tv_hom>0)){

       for (int k = 0; k < p_tv_hom; k++){

         yhat(0) += param_tv_hom(0,k) * x_tv_hom(0,k);

       }

     }
   }

   // residual
   res(0) = y(0) - yhat(0);

   // conditional volatility of nu_t
   // note: this has to be a function of res_0 and condvola_0

   condvola(0) = omega + alpha * pow(res_0,2) + beta * condvola_0;

   // initialize the log-likelihood:
   LogL_contr(0) = (pow(res(0),2)/condvola(0)) + log(condvola(0));



   // iterate over all time series observations

   for (int i = 1; i < n; i++){

     if (p_cst > 0){

       // (a) constant parameters:
       //     note: all parameters of the conditional updating equation are
       //           in the (p_tv_het x npar_tv) matrix: pars.tv.het

       for (int k = 0; k < p_cst; k++){

         // parameters:
         varpi = pars_cst(k,0);
         xi    = pars_cst(k,1);
         c     = pars_cst(k,2);

         // time-varying parameters:
         param_cst(i,k) = varpi + xi * (x_cst(i-1,k)*res(i-1)) + c * param_cst(i-1,k);

         if (q > 0){  // if there are exogenous variables, add their effect here
           gamma_vec = pars_cst.row(k);
           gamma_vec = gamma_vec(3,3+q-1);

           for (int m = 0; m < q; m++){

             param_cst(i,k) += gamma_vec(m) * z(i-1,m);

           }
         }
       }
     }


     if (p_tv > 0){

       // filters of time-varying parameters
       // (b) heteroskedastic regressors:
       //     note: all parameters of the conditional updating equation are
       //           in the (p_tv_het x npar_tv) matrix: pars.tv.het

       if (p_tv_het > 0){

         for (int j = 0; j < p_tv_het; j++){

           // parameters:
           varpi = pars_tv_het(j,0);
           xi    = pars_tv_het(j,1);
           c     = pars_tv_het(j,2);

           // time-varying parameters:
           param_tv_het(i,j) = varpi + xi * ((x_tv_het(i-1,j)*res(i-1))/(pow(mu(j),2) + condvar(i-1,j))) + c * param_tv_het(i-1,j);

           if (q > 0){  // if there are exogenous variables, add their effect here
             gamma_vec = pars_tv_het.row(j);
             gamma_vec = gamma_vec(3,3+q-1);

             for (int m = 0; m < q; m++){

               param_tv_het(i,j) += gamma_vec(m) * z(i-1,m);

             }
           }
         }
       }

       // (c) homoskedastic regressors:
       //     note: - all parameters of the conditional updating equation are in
       //             the (p_tv_hom x npar_tv) matrix: pars_tv_hom
       //           - different updating term

       if (p_tv_hom > 0){

         for (int k = 0; k < p_tv_hom; k++){

           // parameters:
           varpi = pars_tv_hom(k,0);
           xi    = pars_tv_hom(k,1);
           c     = pars_tv_hom(k,2);

           // time-varying regressors:
           param_tv_hom(i,k) = varpi + xi * (x_tv_hom(i-1,k)*res(i-1)) + c * param_tv_hom(i-1,k);

           if (q > 0){  // if there are exogenous variables, add their effect here
             gamma_vec = pars_tv_hom.row(k);
             gamma_vec = gamma_vec(3,3+q-1);

             for (int m = 0; m < q; m++){

               param_tv_hom(i,k) += gamma_vec(m) * z(i-1,m);

             }
           }
         }
       }


       // (d) conditional mean of yhat

       if (p_cst > 0){

         for (int m = 0; m < p_cst; m++){

           yhat(i) += param_cst(i,m) * x_cst(i,m);

         }

       }

       if ((p_tv_het>0)&&(p_tv_hom>0)) {

         for (int j = 0; j < p_tv_het; j++){

           yhat(i) += param_tv_het(i,j) * x_tv_het(i,j);

         }

         for (int k = 0; k < p_tv_hom; k++){

           yhat(i) += param_tv_hom(i,k) * x_tv_hom(i,k);

         }

       } else if ((p_tv_het>0)&&(p_tv_hom==0)) {

         for (int j = 0; j < p_tv_het; j++){

           yhat(i) += param_tv_het(i,j) * x_tv_het(i,j);

         }

       } else if ((p_tv_het==0)&&(p_tv_hom>0)) {

         for (int k = 0; k < p_tv_hom; k++){

           yhat(i) += param_tv_hom(i,k) * x_tv_hom(i,k);

         }
       }

     }

     // (e) residual
     res(i) = y(i) - yhat(i);


     // (f) conditional volatility of nu_t
     //     note: this is a function of res_t-1 and condvola_t-1

     condvola(i) = omega + alpha * pow(res(i-1),2) + beta * condvola(i-1);


     // compute negative log-likelihood: this is adds up the likelihood contributions
     // of each period t

     LogL_contr(i) = (pow(res(i),2)/condvola(i)) + log(condvola(i));

   }

   return LogL_contr;
 }





#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]


 //' @title Likelihood for ACB model with Gaussian innovations for all model parameters
 //' @description This function computes the negative loglikelihood for the ACB model
 //' @param pars Vector of parameters (param.cst, GARCH.pars, param.tv)
 //' @param fixed_id vector specifying the position of parameters to be fixed
 //' @param est_id vector specifying the position of parameters to be estimated
 //' @param fixed_vals vector of fixed values to be imposed in the pars vector
 //' @param y Dependent variable (vector)
 //' @param x_c Regressor with constant parameter (matrix)
 //' @param x_het Heteroskedastic regressor with time-varying param (matrix)
 //' @param x_hom Homoskedastic regressors with time-varying param (matrix)
 //' @param z Exogenous variables (matrix)
 //' @param init initialization of the filters (vector)
 //' @param p_cst number of constant parameters
 //' @param p_tv_het number of time-varying parameters of heteroskedastic reg.
 //' @param p_tv_hom number of time-varying parameters of homoskedastic reg.
 //' @param npar_GARCH number of GARCH parameters
 //' @param npar_tv number of parameters in tv param filter
 //' @param q number of exogenous variables
 //' @param k_full number of parameters in ACB in full parameter case
 //' @importFrom Rcpp sourceCpp
 //' @useDynLib ACB
 //' @keywords internal
 //' @export
 // [[Rcpp::export]]
 double ACB_likl_complete(arma::vec pars, arma::vec fixed_id, arma::vec est_id,
                          arma::vec fixed_vals, arma::colvec y, arma::mat x_c,
                          arma::mat x_het, arma::mat x_hom, arma::mat z,
                          arma::vec init,
                          double p_cst, double p_tv_het, double p_tv_hom,
                          double npar_GARCH, double npar_tv, double q,
                          double k_full)
 {

   int n = y.n_elem;                            // number of observations
   //int p = p_cst + p_tv_het + p_tv_hom;       // number of regressors

   int p_tv = p_tv_het + p_tv_hom;  // number of reg. with time-varying pars

   int npar_uGARCH = p_tv_het * 4;              // number of first step coef

   // Initialize matrices, vectors and variables
   arma::mat x_cst, x_tv_het, x_tv_hom, pars_uGARCH_vec;
   arma::mat pars_uGARCH, pars_ACB, pars_ACB_vec, pars_cst_vec, pars_cst;
   arma::mat pars_tv_vec, pars_tv, pars_tv_het, pars_tv_hom;
   arma::mat param_cst    = arma::zeros<arma::mat>(n, p_cst);
   arma::mat param_tv_het = arma::zeros<arma::mat>(n, p_tv_het);
   arma::mat param_tv_hom = arma::zeros<arma::mat>(n, p_tv_hom);
   arma::mat condvola     = arma::zeros<arma::mat>(n, 1);
   arma::mat res          = arma::zeros<arma::mat>(n, 1);
   arma::mat yhat         = arma::zeros<arma::mat>(n, 1);
   arma::mat pars_full    = arma::zeros<arma::mat>(k_full, 1);
   arma::mat condvar      = arma::zeros<arma::mat>(n, p_tv_het);
   arma::vec param_tv_0, gamma_vec, mu;
   double omega, alpha, beta, k_fixed, k_est, res_0, g2_0, condvola_0;
   double mu_j, omega_j, alpha_j, beta_j, varpi, xi, c, LogL;


   // regressors
   if (p_cst > 0){
     x_cst    = x_c;
   }

   if (p_tv_het > 0){
     x_tv_het = x_het;
   }

   if (p_tv_hom > 0){
     x_tv_hom = x_hom;
   }

   if (q > 0){
     z = z;
   }

   // step 1: estimate univariate GARCH model parameters by MLE:
   // generate conditional volatilities of the regressors given pars_uGARCH

   if (p_tv_het > 0){

     // univariate GARCH model parameters (4 for each heterosked. regressor)
     // called pars.1s in R
     pars_uGARCH_vec = pars.subvec(0,npar_uGARCH-1);

     // remaining ACB model parameters
     // called pars.2s in R
     pars_ACB_vec = pars.subvec(npar_uGARCH,pars.n_elem - 1);

     // reshape pars vector to a (p_tv_het x 4) matrix
     pars_uGARCH = reshape(pars_uGARCH_vec, 4, p_tv_het);
     pars_uGARCH = pars_uGARCH.t();

     // read out constant conditional mean of regressors mu
     mu = pars_uGARCH.col(0);

     // get conditional volatilies for all p_tv_het regressors j

     for (int j = 0; j < p_tv_het; j++){

       // GARCH parameters:
       mu_j    = pars_uGARCH(j,0);
       omega_j = pars_uGARCH(j,1);
       alpha_j = pars_uGARCH(j,2);
       beta_j  = pars_uGARCH(j,3);

       // initialize variance of res: g2
       g2_0 = omega_j/(1 - alpha_j - beta_j); // set to uncond. variance
       res_0 = 0;                             // set to arbitrary value

       // initialize conditional vola for t=1:
       condvar(0,j) = omega_j + alpha_j *pow(res_0,2) + beta_j * g2_0;

       // iterate over all time periods:
       for (int i = 1; i < n; i++){

         condvar(i,j) = omega_j + alpha_j * pow((x_het(i-1,j) - mu_j),2) + beta_j * condvar(i-1,j);

       }

     }


   } else {

     // ACB model parameters
     // called pars.2s in R
     pars_ACB_vec = pars;

   }


   // augmented the second step parameter vector (pars_ACB_vec) to be estimated
   // by the fixed values

   k_fixed = fixed_id.n_elem;
   k_est   = est_id.n_elem;

   for (int k = 0; k < k_fixed; k++){
     // impose the parameters that are estimated to the pars vector
     int idx = fixed_id(k);

     pars_full.row(idx) =  fixed_vals(k);

   }

   for (int k = 0; k < k_est; k++){
     // impose the fixed values to the pars_ACB_vec vector
     int idx = est_id(k);

     pars_full.row(idx) =  pars_ACB_vec(k);

   }

   // read out parameters (GARCH_param, param_cst,  param_tv)

   // GARCH parameters
   omega = pars_full(0);
   alpha = pars_full(1);
   beta  = pars_full(2);


   // constant parameters: param_cst
   if (p_cst > 0){
     pars_cst_vec = pars_full.rows(npar_GARCH,(p_cst*(3+q)+npar_GARCH-1));

     // reshape as matrix of dimension (p_cst x npar_tv)
     pars_cst = reshape(pars_cst_vec, npar_tv, p_cst);
     pars_cst = pars_cst.t();
   }

   // time-varying parameters
   if (p_tv > 0){
     pars_tv_vec = pars_full.rows((p_cst*(3+q)+npar_GARCH), (k_full-1));


     // reshape as matrix of dimension (p_tv x npar_tv)
     pars_tv = reshape(pars_tv_vec, npar_tv, p_tv);
     pars_tv = pars_tv.t();


     // separate for heteroskedastic and homoskedastic regressors
     if ((p_tv_het>0)&&(p_tv_hom>0)) {

       pars_tv_het = pars_tv.rows(0,p_tv_het-1);
       pars_tv_hom = pars_tv.rows(p_tv_het,p_tv-1);

     } else if ((p_tv_het>0)&&(p_tv_hom==0)){

       pars_tv_het = pars_tv;

     } else if ((p_tv_het==0)&&(p_tv_hom>0)){

       pars_tv_hom = pars_tv;

     }
   }


   // read out initial values for the filters for t=1
   res_0 = 0.01;     // initial residual is set to zero

   if (p_cst > 0){

     param_cst.row(0) = pars_cst.col(0).t();

   }

   // param_tv filters: first p_tv elements in init
   if (p_tv > 0){

     param_tv_0 = init.subvec(0,p_tv-1);

     if ((p_tv_het>0)&&(p_tv_hom>0)){
       param_tv_het.row(0) = param_tv_0.subvec(0,p_tv_het-1).t();
       param_tv_hom.row(0) = param_tv_0.subvec(p_tv_het,p_tv-1).t();
     } else if ((p_tv_het>0)&&(p_tv_hom==0)){
       param_tv_het.row(0) = param_tv_0.t();
     } else if ((p_tv_het==0)&&(p_tv_hom>0)){
       param_tv_hom.row(0) = param_tv_0.t();
     }

   }

   // conditional mean yhat: impose the impact of the constant parameters on all
   // periods t of yhat if p_cst > 0

   if (p_cst>0){
     for (int m = 0; m < p_cst; m++){

       yhat(0) += param_cst(0,m) * x_cst(0,m);

     }
   }

   // conditional variance of res
   condvola_0 = init(init.n_elem-1);

   // initialize yhat(0)

   if (p_tv > 0){

     if ((p_tv_het>0)&&(p_tv_hom>0)){

       for (int j = 0; j < p_tv_het; j++){

         yhat(0) += param_tv_het(0,j) * x_tv_het(0,j);

       }

       for (int k = 0; k < p_tv_hom; k++){

         yhat(0) += param_tv_hom(0,k) * x_tv_hom(0,k);

       }


     } else if ((p_tv_het>0)&&(p_tv_hom==0)){

       for (int j = 0; j < p_tv_het; j++){

         yhat(0) += param_tv_het(0,j) * x_tv_het(0,j);

       }

     } else if ((p_tv_het==0)&&(p_tv_hom>0)){

       for (int k = 0; k < p_tv_hom; k++){

         yhat(0) += param_tv_hom(0,k) * x_tv_hom(0,k);

       }

     }
   }

   // residual
   res(0) = y(0) - yhat(0);

   // conditional volatility of nu_t
   // note: this has to be a function of res_0 and condvola_0

   condvola(0) = omega + alpha * pow(res_0,2) + beta * condvola_0;

   // initialize the log-likelihood:
   LogL = (pow(res(0),2)/condvola(0)) + log(condvola(0));



   // iterate over all time series observations

   for (int i = 1; i < n; i++){

     if (p_cst > 0){

       // (a) constant parameters:
       //     note: all parameters of the conditional updating equation are
       //           in the (p_tv_het x npar_tv) matrix: pars.tv.het

       for (int k = 0; k < p_cst; k++){

         // parameters:
         varpi = pars_cst(k,0);
         xi    = pars_cst(k,1);
         c     = pars_cst(k,2);

         // time-varying parameters:
         param_cst(i,k) = varpi + xi * (x_cst(i-1,k)*res(i-1)) + c * param_cst(i-1,k);

         if (q > 0){  // if there are exogenous variables, add their effect here
           gamma_vec = pars_cst.row(k);
           gamma_vec = gamma_vec(3,3+q-1);

           for (int m = 0; m < q; m++){

             param_cst(i,k) += gamma_vec(m) * z(i-1,m);

           }
         }
       }
     }


     if (p_tv > 0){

       // filters of time-varying parameters
       // (b) heteroskedastic regressors:
       //     note: all parameters of the conditional updating equation are
       //           in the (p_tv_het x npar_tv) matrix: pars.tv.het

       if (p_tv_het > 0){

         for (int j = 0; j < p_tv_het; j++){

           // parameters:
           varpi = pars_tv_het(j,0);
           xi    = pars_tv_het(j,1);
           c     = pars_tv_het(j,2);

           // time-varying parameters:
           param_tv_het(i,j) = varpi + xi * ((x_tv_het(i-1,j)*res(i-1))/(pow(mu(j),2) + condvar(i-1,j))) + c * param_tv_het(i-1,j);

           if (q > 0){  // if there are exogenous variables, add their effect here
             gamma_vec = pars_tv_het.row(j);
             gamma_vec = gamma_vec(3,3+q-1);

             for (int m = 0; m < q; m++){

               param_tv_het(i,j) += gamma_vec(m) * z(i-1,m);

             }
           }
         }
       }

       // (c) homoskedastic regressors:
       //     note: - all parameters of the conditional updating equation are in
       //             the (p_tv_hom x npar_tv) matrix: pars_tv_hom
       //           - different updating term

       if (p_tv_hom > 0){

         for (int k = 0; k < p_tv_hom; k++){

           // parameters:
           varpi = pars_tv_hom(k,0);
           xi    = pars_tv_hom(k,1);
           c     = pars_tv_hom(k,2);

           // time-varying regressors:
           param_tv_hom(i,k) = varpi + xi * (x_tv_hom(i-1,k)*res(i-1)) + c * param_tv_hom(i-1,k);

           if (q > 0){  // if there are exogenous variables, add their effect here
             gamma_vec = pars_tv_hom.row(k);
             gamma_vec = gamma_vec(3,3+q-1);

             for (int m = 0; m < q; m++){

               param_tv_hom(i,k) += gamma_vec(m) * z(i-1,m);

             }
           }
         }
       }


       // (d) conditional mean of yhat

       if (p_cst > 0){

         for (int m = 0; m < p_cst; m++){

           yhat(i) += param_cst(i,m) * x_cst(i,m);

         }

       }

       if ((p_tv_het>0)&&(p_tv_hom>0)) {

         for (int j = 0; j < p_tv_het; j++){

           yhat(i) += param_tv_het(i,j) * x_tv_het(i,j);

         }

         for (int k = 0; k < p_tv_hom; k++){

           yhat(i) += param_tv_hom(i,k) * x_tv_hom(i,k);

         }

       } else if ((p_tv_het>0)&&(p_tv_hom==0)) {

         for (int j = 0; j < p_tv_het; j++){

           yhat(i) += param_tv_het(i,j) * x_tv_het(i,j);

         }

       } else if ((p_tv_het==0)&&(p_tv_hom>0)) {

         for (int k = 0; k < p_tv_hom; k++){

           yhat(i) += param_tv_hom(i,k) * x_tv_hom(i,k);

         }
       }

     }

     // (e) residual
     res(i) = y(i) - yhat(i);


     // (f) conditional volatility of nu_t
     //     note: this is a function of res_t-1 and condvola_t-1

     condvola(i) = omega + alpha * pow(res(i-1),2) + beta * condvola(i-1);


     // compute negative log-likelihood: this is adds up the likelihood contributions
     // of each period t

     LogL += (pow(res(i),2)/condvola(i)) + log(condvola(i));

   }

   LogL = LogL;

   return LogL;
 }







