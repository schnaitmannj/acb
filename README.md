# ACB

This package allows to estimate the ACB model introduced in the paper "Autoregressive conditional betas" by Blasques, Francq and Laurent (2023) forthcoming in the Journal of Econometrics. 
The paper introduces an autoregressive conditional beta (ACB) model that allows regressions with dynamic betas (or slope coefficients) and residuals with GARCH conditional volatility. The model fits in the (quasi) score-driven approach recently proposed in the literature, and it is semi-parametric in the sense that the distributions of the innovations are not necessarily specified. The time-varying betas are allowed to depend on past shocks and exogenous variables


Installation
------------

### GitLab (development)

The latest version of the package is under development at
[GitLab](https://gitlab.com/schnaitmannj/acb). You can install the
development version using these commands:

    install.packages("devtools")
    devtools::install_gitlab("schnaitmannj/acb")

If you are using Windows, you need to install the
[Rtools](https://cran.r-project.org/bin/windows/Rtools/) for compilation
of the codes.

Note: on some machines the packages "magic" is not automatically installed. If the installation fails because of this, please manually install "magic" first and then install this package.

Examples
--------

    # Conducts a small simulation design and estimates the ACB model
 
    # (1) specify the simulation setting:
 
    p   <- 4                    # 4 regressors x_it
    N   <- 4000                 # length of simulated time series
    tau <- 1000                 # burn-in period
    z   <- NULL                 # no exogenous regressors in tv param filters
 
    int            <- TRUE      # intercept, x_1t = 1
    constant.param <- c(1)      # which has a constant parameter
    param.cst      <- c(0.1)    # equal to 0.1
 
    pars.ACB       <- NULL      # use default values for tv param filters:
                                # xi.i = 0.02, c.i = 0.99,
                                # varpi.2 = 0.01, varpi.3 = 0.005, varpi.4 = 0
 
    pars.uGARCH    <- NULL      # use default values for univariate GARCHs:
                                # mu.i = 0, omega.i = 0.02, alpha.i = 0.08,
                                # beta.i = 0.9 -> uncond.var.i = 1
    GARCH.pars     <- NULL      # use default values for GARCH(1,1) of y/ nu:
                                # omega = 0.005, alpha = 0.05, beta = 0.94
  
    homosked.reg   <- NULL      # no homoskedastic regressor (apart from int)
 
    # (2) run the simulation
    sim.out <- Simulate.ACB( pars.uGARCH = pars.uGARCH, param.cst = param.cst,
                             GARCH.pars = GARCH.pars, pars.ACB = pars.ACB,
                             N = N, tau = tau, p = p, z = z, int = int,
                             constant.param = constant.param,
                             homosked.reg = homosked.reg )
 
    # (3) estimate the ACB model
 
    y <- sim.out$data.sim$y.sim
    x <- sim.out$data.sim$x.sim
    z <- NULL
   
    ACB.sim <- Estimate.ACB(y, x, z, constant.param = constant.param,
                            init.local = 1, num.iter = 3)
 
    # (4) compare true underlying parameters and estimated parameters
 
    true.pars    <- sim.out$true.pars
    est.pars.1s  <- ACB.sim$coef.1s
    est.pars.2s  <- ACB.sim$coef.2s
 
    print("true parameters")
    print(true.pars)
 
    print("first step parameters: univariate GARCH pars")
    print(est.pars.1s)
 
    print("second step parameters: ACB pars")
    print(est.pars.2s)

    # (5) estimate the covariance matrix of the estimated parameters.
    ACB.cov <- Cov.ACB(ACB.sim, method = "empirical")
 
    # read out the estimated parameters and compute standard errors
    pars.1s.mat  <- ACB.sim$coef.1s      # coef.1s is a matrix
    pars.1s  <- matrix(t(pars.1s.mat),ncol=1)
    pars.2s  <- ACB.sim$solver.2s$pars   # coef.2s as vector
    pars <- c(pars.1s,pars.2s)
  
    se.ACB <- sqrt(diag(ACB.cov$sigma))
 
    print("estimated parameters and associated standard errors")
    print(rbind(pars, se.ACB))

References
----------

[Autoregressive conditional betas](https://www.dropbox.com/scl/fi/86kw8ri3qefa09htyptew/ACB_complete_JOE_Final2.pdf?rlkey=58jyyvygi9i8z251uhdc31fqj&dl=0)
